#ifndef COUNTER_H
#define COUNTER_H

#include <QObject>

class Counter: public QObject
{
    Q_OBJECT
    size_t count_;
    std::string name_;
public:
    Counter(std::string name, QObject* parent = nullptr);
    size_t count() const;
    bool connect(const Counter& oth) const;
public slots:
    void setCount(size_t count);
    signals:
    void countChanged(size_t count);
};

#endif // COUNTER_H
