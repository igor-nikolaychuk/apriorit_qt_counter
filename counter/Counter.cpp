#include "Counter.h"
#include <QDebug>

Counter::Counter(std::string name, QObject* parent)
    : count_(0), name_(std::move(name)), QObject(parent) { }

size_t Counter::count() const
{
    return count_;
}

bool Counter::connect(const Counter &oth) const
{
    return QObject::connect(this, &Counter::countChanged, &oth, &Counter::setCount);
}

void Counter::setCount(size_t count)
{
    count_ = count;
    countChanged(count_);
    qDebug() << name_.c_str() << " count set to " << count;
}
