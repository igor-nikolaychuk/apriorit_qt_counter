#include <QCoreApplication>
#include "Counter.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Counter first("first"),
            second("second");

    first.setCount(5);
    second.setCount(42);

    assert(first.connect(second));
    first.setCount(6);

    assert(first.count() == second.count());

    return a.exec();
}
